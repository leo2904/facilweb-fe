<?php
$vidsoftware = "";
$vtoken      = "";
$vnit        = "";

if(isset($_GET["idsoftware"]))
{
	$vidsoftware = $_GET["idsoftware"];
}

if(isset($_GET["token"]))
{
	$vtoken = $_GET["token"];
}

if(isset($_GET["nit"]))
{
	$vnit  = $_GET["nit"];
}

if(!empty($vidsoftware) and !empty($vtoken) and !empty($vnit))
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://www.facilwebnube.com/apidian2021/public/api/ubl2.1/numbering-range',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS =>'{
		"Nit": "'.$vnit.'",
		"IDSoftware": "'.$vidsoftware.'"
	}
	',
	  CURLOPT_HTTPHEADER => array(
		'Content-Type: application/json',
		'accept: application/json',
		'Authorization: Bearer '.$vtoken
	  ),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	echo $response;
}
?>



